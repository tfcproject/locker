const fetch = require("node-fetch");
const express=require('express')
const mongoose = require('mongoose');
const path = require('path');
// mongoose.connect('mongodb://localhost:27017/meikarta', {useNewUrlParser: true, useUnifiedTopology: true});

// logSchema=new mongoose.Schema({
//    uid:{type: mongoose.Schema.Types.ObjectId, ref:'user'},
//    action:{
//     type: String,
//     enum: ['in', 'out']}
// }, {timestamps:true})

const user = mongoose.model('user', { name: String });
// const inLog = mongoose.model('in', logSchema);
// const outLog = mongoose.model('out',logSchema);
// const log = mongoose.model('log',logSchema);

const app= express()

app.use(express.static(__dirname + '/view'));
app.get('/',(req, res)=>{
    res.render('index')
})

app.get('/display',(req, res)=>{
  res.sendFile(path.join(__dirname+'/view/display.html'));
})
// app.get('/sumLog',(res,req)=>{
//   log.aggregate().group({_id:"$action", count:{$sum: 1},}).then((data)=>{
//     req.status(200).send({success:true, data:data})
//   })
// })

// app.get('/logs',(res,req)=>{
//   log.find().populate('uid').then((data)=>{
//     req.status(200).send({success:true, data:data})
//   })
// })

const httpServer = require('http').createServer(app);
const io = require('socket.io').listen(httpServer);



httpServer.listen(3000)

const mqtt = require('mqtt');
const fs = require('fs');
var db

//Connect to MQTT
const options = {
  key: fs.readFileSync('f09c66d861-private.pem.key'),
  cert: fs.readFileSync('f09c66d861-certificate.pem.crt'),
  ca: [fs.readFileSync('amazonRootCA1.pem')],
  checkServerIdentity: () => null,
};

const mqttClient = mqtt.connect(
  'mqtts://a3n6w4uikjefzl-ats.iot.ap-southeast-1.amazonaws.com',
  options,
);


//Start Socket
io.on('connection', socket => {
  console.log('a user is connected');
  socket.on('disconnect', () => {
    console.log(socket.channel, ' is disconnected');
  });
  //Listen to gate event from client's browser
  socket.on('scan', a => {
    console.log('hello')
    mqttClient.publish('$aws/things/meikarta/gate', 'i1');
    io.emit('open',Math.round( Math.random()*100))
  });
  
});

// // Connect to mongodb
// const MongoClient = require('mongodb').MongoClient

// MongoClient.connect('mongodb://localhost:27017', (err, client) => {
//   if (err) return console.log(err)
//   db = client.db('config')
//   httpServer.listen(3000,(req,res)=>console.log('Server started'))

// })

//On Connect subscribe to meikarta topic MQTT
mqttClient.on('connect',(a,b,c)=>{
  mqttClient.subscribe('$aws/things/meikarta/auth',{qos:1, });
})
mqttClient.on('disconnect', ()=>{
  mqttClient.reconnect()
});

// //On meikarta topic check user id -> open gate and log to front end if found
// mqttClient.on('message',async (topic, message)=>{
//   var [uid,action]= message.toString().split('-')
//   console.log(uid, action)
//   if (action==null){
//     console.log('action null')
//     // return  mqttClient.publish('$aws/things/meikarta/gate', 'er');
//   }
//   else if (action.length!==2){
//     console.log('action invalid')
//     // return  mqttClient.publish('$aws/things/meikarta/gate', 'er');}
//   }
//   else if (topic==='$aws/things/meikarta/auth'){
//     // user.findById(uid).then((u)=>{
//       // console.log(u)
//       // if (u){
//         mqttClient.publish('$aws/things/meikarta/gate', action);
//         console.log(`Gate Opened ${message.toString()} ${action}`)
//         io.emit('log', `${u.name}-${action}`)
//       //   if(action[0]==='i'){
//       //     console.log('log saved to db')
//       //     const newIn=new log({uid,action:'in'})
//       //     newIn.save()
//       //   }
//       //   else if(action[0]==='o'){
//       //     console.log('log saved to db')
//       //     const newOut=new log({uid,action:'out'})
//       //   newOut.save()
//       //   }
//       //   else{
//       //     console.log('no log')
//       //   }
//         //Can consider pushing the log to mongodb for future reference
//       }
//       else{
//         console.log(`Intruder ${message.toString()}`)
//         io.emit('log', 'er')
//         return  mqttClient.publish('$aws/things/meikarta/gate', `d${action[1]}`);
//       }
//       }).catch((er)=>{
//         console.log('er-i1')
//         io.emit('log', 'er-i1')

//       })
//     }
//       else{
//         console.log(`Gate denied ${message.toString()}`)
//         io.emit('log', 'er')
//         return  mqttClient.publish('$aws/things/meikarta/gate', `d${action[1]}`);
//       }
//     })









