# Server Documentation

Run Mongo and make sure it is running on port ```27017```

To start the app run 

```node app.js```


# Swing Gate MQTT Documentation


## MQTT Server Endpoint
```a3n6w4uikjefzl-ats.iot.ap-southeast-1.amazonaws.com```

## Topics
- $aws/things/tfc/gate
- $aws/things/tfc/auth

## Usage

| From  | to | Topic | Message | Action |
|-------|----|-------|---------|--------|
|Server|Gate|$aws/things/tfc/gate| action | Publish
|Gate|Server|$aws/things/tfc/auth| ```uid```-```action```| Authenticate user, emit log to connected clients, and publishes action to $aws/things/tfc/gate


Action is a two-letters word

- First word may be i for in, o for out, or d for denied
- Second word may be 1 for gate 1 or 2 for gate 2
- Action will be 'er' if the input is invalid

## Registered Users
| UID  | Name | 
|-------|----|
|3104|Wida|
|10223|reynold|

