# Server Documentation

Run Mongo and make sure it is running on port ```27017```

To start the app run 

```node app.js```


# Swing Gate MQTT Documentation


## MQTT Server Endpoint
```a3n6w4uikjefzl-ats.iot.ap-southeast-1.amazonaws.com```

## Topics
- $aws/things/temp
- $aws/things/humidity
- $aws/things/pump
- $aws/things/tfc/pumpSwitch

## Usage

| From  | to | Topic | Message | Action |
|-------|----|-------|---------|--------|
|Sensor|Server|$aws/things/temp| temp level | Publish Insert to db
|Sensor|Server|$aws/things/humidity| humidity level | Publish Insert to db
|Sensor|Server|$aws/things/moist| soil moisture level | Publish Insert to db 
|Pump|Server|$aws/things/pump| 0/1 | Publish
|Server|Pump|$aws/things/tfc/pumpswitch| 0/1 | Publish