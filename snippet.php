<?php

/* Construct a new client instance, passing a client ID of “MyClient” */
$client = new Mosquitto\Client('MyClient');

/* Set the callback fired when the connection is complete */
$client->onConnect(function($code, $message) use ($client) {
    /* Subscribe to the broker's namespace, which shows debugging info */
    $client->subscribe('$aws/things/tfc/auth', 0);
});

/* Set the callback fired when we receive a message */
$client->onMessage(function($message) use ($client) {
    /* Display the message's topic and payload */
    echo $message->topic, "\n", $message->payload, "\n\n";
  
  //TODO: Check Database for User and save to user
     $client->publish('$aws/things/tfc/gate', action);
  //TODO: Push Socket IO to client
  // io.emit('log', 'Gate opened for'. $user->name . $message)
});

/* Connect, supplying the host and port. */
/* If not supplied, they default to localhost and port 1883 */
$client->setTlsCertificates('/path/to/ca.crt', '/path/to/client.crt', '/path/to/client.key', 'passphrase');
$client->connect('mqtts://a3n6w4uikjefzl-ats.iot.ap-southeast-1.amazonaws.com', 8883);



/* Enter the event loop */
$client->loopForever();